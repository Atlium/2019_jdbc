package io.humb1t.dao;

import io.humb1t.utils.file.FileFactory;
import io.humb1t.utils.file.FileSource;
import io.humb1t.utils.file.FileUtils;

public class DbConfig {

    private static final String CREATE_SCRIPT_FILE = "create.sql";
    private static final String CREATE_FUNC_SCRIPT_FILE = "create_func_and_trigger.sql";
    private static final String INSERT_SCRIPT_FILE = "insert.sql";
    private static final String DROP_FUNC_SCRIPT_FILE = "drop_func_and_trigger.sql";
    private static final String DELETE_SCRIPT_FILE = "delete.sql";
    private static final String DROP_SCRIPT_FILE = "drop.sql";
    private static final FileFactory FILE_FACTORY;
    static String CREATE_FUNC_SCRIPT;
    static String CREATE_SCRIPT;
    static String INSERT_SCRIPT;
    static String DROP_FUNC_SCRIPT;
    static String DELETE_SCRIPT;
    static String DROP_SCRIPT;

    private DbConfig() {
        //constants
    }

    static {
        FILE_FACTORY = new FileFactory();
        CREATE_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, CREATE_SCRIPT_FILE));
        CREATE_FUNC_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, CREATE_FUNC_SCRIPT_FILE));
        INSERT_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, INSERT_SCRIPT_FILE));
        DROP_FUNC_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, DROP_FUNC_SCRIPT_FILE));
        DELETE_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, DELETE_SCRIPT_FILE));
        DROP_SCRIPT = FileUtils.getTextFromFile(FILE_FACTORY.getFile(FileSource.RESOURCE, DROP_SCRIPT_FILE));
    }

}
