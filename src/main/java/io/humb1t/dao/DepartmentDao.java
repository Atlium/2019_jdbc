package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;

import java.util.List;

public interface DepartmentDao {
    DepartmentEntity create(DepartmentEntity manEntity);

    boolean remove(Long id);

    DepartmentEntity update(DepartmentEntity manEntity);

    List<DepartmentEntity> findAll();

    DepartmentEntity find(Long id);

    List<DepartmentEntity> findMostPopular();
}
