DROP TRIGGER on_delete_department ON employees;
DROP FUNCTION check_employee_dismissed();
DROP FUNCTION getEmployeesByDepartmentId(BIGINT);