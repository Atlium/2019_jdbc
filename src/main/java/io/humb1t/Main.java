package io.humb1t;

import io.humb1t.utils.db.ConnectionFactory;
import io.humb1t.utils.db.RequestWrapper;
import io.humb1t.utils.file.FileFactory;
import io.humb1t.utils.file.FileSource;
import io.humb1t.utils.file.FileUtils;
import org.apache.log4j.Logger;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);
    private static final String DELETE_FUNC_SCRIPT_FILE = "drop_func_and_trigger.sql";
    private static final String DROP_SCRIPT_FILE = "drop.sql";


    public static void main(String[] args) {
        removeAllTablesAndFunctions();
    }

    /**
     * clear db if something going wrong way
     */
    private static void removeAllTablesAndFunctions() {
        FileFactory fileFactory = new FileFactory();
        String dropFuncScript = FileUtils.getTextFromFile(fileFactory.getFile(FileSource.RESOURCE, DELETE_FUNC_SCRIPT_FILE));
        String dropTablesScript = FileUtils.getTextFromFile(fileFactory.getFile(FileSource.RESOURCE, DROP_SCRIPT_FILE));
        RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            try {
                connection.prepareStatement(dropFuncScript).executeUpdate();
            } catch (RuntimeException e) {
                LOGGER.error("Error while removing all tables and functions", e);
            }
            try {
                connection.prepareStatement(dropTablesScript).executeUpdate();
            } catch (RuntimeException e) {
                LOGGER.error("Error while removing all tables and functions", e);
            }
            return null;
        });
    }
}
