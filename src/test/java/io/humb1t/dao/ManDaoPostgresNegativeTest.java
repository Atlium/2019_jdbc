package io.humb1t.dao;

import io.humb1t.domain.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.assertEquals;

public class ManDaoPostgresNegativeTest {
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        manDao = new ManDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DROP_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            deleteDataStatement.executeUpdate();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullMan() {
        manDao.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createManNullId() throws ParseException {
        ManEntity manEntity = new ManEntity(null, "qwe", "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.create(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createManNullFirstName() throws ParseException {
        ManEntity manEntity = new ManEntity(978L, null, "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.create(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createManNullSecondName() throws ParseException {
        ManEntity manEntity = new ManEntity(978L, "qwe", null,
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.create(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createManNullBirthDay() {
        ManEntity manEntity = new ManEntity(978L, "qwe", "rty", null);
        manDao.create(manEntity);
    }

    @Test(expected = ExecutionException.class)
    public void createManExistingId() throws ParseException {
        ManEntity manEntity = new ManEntity(1L, "qwe", "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.create(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNullMan() {
        manDao.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullMan() {
        manDao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateManNullId() throws ParseException {
        ManEntity manEntity = new ManEntity(null, "qwe", "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.update(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateManNullFirstName() throws ParseException {
        ManEntity manEntity = new ManEntity(1L, null, "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.update(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateManNullSecondName() throws ParseException {
        ManEntity manEntity = new ManEntity(1L, "qwe", null,
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.update(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateManNullBirthDay() {
        ManEntity manEntity = new ManEntity(1L, "qwe", "rty", null);
        manDao.update(manEntity);
    }

    @Test(expected = ExecutionException.class)
    public void updateManNotExistingId() throws ParseException {
        ManEntity manEntity = new ManEntity(978L, "qwe", "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        manDao.update(manEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findNullMan() {
        manDao.find(null);
    }

    @Test(expected = NotFoundException.class)
    public void findNoOneMan() {
        manDao.find(8L);
    }
}