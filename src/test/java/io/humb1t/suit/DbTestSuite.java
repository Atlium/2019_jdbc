package io.humb1t.suit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ConnectionTestSuite.class, DaoTestSuite.class})
public class DbTestSuite {
}
