package io.humb1t.domain;

import io.humb1t.utils.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class EmployeeEntity implements Serializable {

    private static final long serialVersionUID = 1467620114423006553L;
    private Long id;
    private Date employmentDate;
    private ManEntity man;
    private DepartmentEntity department;
    private Date dismissDate;

    public EmployeeEntity() {
    }

    public EmployeeEntity(long id) {
        this.id = id;
    }

    public EmployeeEntity(Long id, Date employmentDate, ManEntity man, DepartmentEntity department, Date dismissDate) {
        this.id = id;
        this.employmentDate = employmentDate;
        this.man = man;
        this.department = department;
        this.dismissDate = dismissDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ManEntity getMan() {
        return man;
    }

    public void setMan(ManEntity man) {
        this.man = man;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public Date getDismissDate() {
        return dismissDate;
    }

    public void setDismissDate(Date dismissDate) {
        this.dismissDate = dismissDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeeEntity)) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getEmploymentDate(), that.getEmploymentDate()) &&
                Objects.equals(getMan(), that.getMan()) &&
                Objects.equals(getDepartment(), that.getDepartment()) &&
                Objects.equals(getDismissDate(), that.getDismissDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getEmploymentDate(), getMan(), getDepartment(), getDismissDate());
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", employmentDate=" + DateUtils.formatToTimestamp(employmentDate) +
                ", man=" + man +
                ", department=" + department +
                ", dismissDate=" + DateUtils.formatToTimestamp(dismissDate) +
                '}';
    }
}
