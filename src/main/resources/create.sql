CREATE TABLE mans
(
  id          BIGINT       NOT NULL    PRIMARY KEY,
  first_name  VARCHAR(255) NOT NULL,
  second_name VARCHAR(255) NOT NULL,
  birth_date  TIMESTAMP    NOT NULL
);
CREATE TABLE departments
(
  ID              BIGINT       NOT NULL    PRIMARY KEY,
  dept_name       VARCHAR(255) NOT NULL,
  foundation_date TIMESTAMP    NOT NULL
);
CREATE TABLE employees
(
  id              BIGINT    NOT NULL    PRIMARY KEY,
  employment_date TIMESTAMP NOT NULL,
  man             BIGINT    NOT NULL,
  department      BIGINT    NOT NULL,
  dismiss_date    TIMESTAMP,
  CONSTRAINT employee_e_man_e_id_fk FOREIGN KEY (man) REFERENCES mans (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT employee_e_department_e_id_fk FOREIGN KEY (department) REFERENCES departments (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT employee_check_dates CHECK (dismiss_date >= employment_date)
);

CREATE OR REPLACE FUNCTION check_employee_born_date()
  RETURNS TRIGGER AS
$cua$
BEGIN
  IF NEW.employment_date < (
    SELECT m.birth_date
    FROM mans m
    WHERE m.id = NEW.man
    LIMIT (1)
  )
  THEN
    RAISE EXCEPTION 'Man birth day cannot be later than hiring date';
  END IF;
  RETURN NEW;
END;
$cua$
LANGUAGE plpgsql;

CREATE TRIGGER on_create_or_update_employee_check_born_date_below_employment_date
  BEFORE INSERT OR UPDATE
  ON employees
  FOR EACH ROW
EXECUTE PROCEDURE check_employee_born_date();

CREATE OR REPLACE FUNCTION check_department_foundation_below_hiring_date()
  RETURNS TRIGGER AS
$cua$
BEGIN
  IF NEW.employment_date < (
    SELECT d.foundation_date
    FROM departments d
    WHERE d.id = NEW.department
    LIMIT (1)
  )
  THEN
    RAISE EXCEPTION 'Employment date cannot be earlier than department foundation date';
  END IF;
  RETURN NEW;
END;
$cua$
LANGUAGE plpgsql;

CREATE TRIGGER on_create_or_update_employee_check_department_foundation_date_below_employment_date
  BEFORE INSERT OR UPDATE
  ON employees
  FOR EACH ROW
EXECUTE PROCEDURE check_department_foundation_below_hiring_date();