package io.humb1t.exception;

public class ConnectException extends RuntimeException {
    public ConnectException(Throwable cause) {
        super(cause);
    }
}
