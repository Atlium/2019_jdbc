package io.humb1t.dao;

import io.humb1t.domain.EmployeeEntity;

import java.util.List;

public interface EmployeeDao {
    EmployeeEntity create(EmployeeEntity employeeEntity);

    boolean remove(Long id);

    EmployeeEntity update(EmployeeEntity employeeEntity);

    List<EmployeeEntity> findAll();

    EmployeeEntity find(Long id);

    List<EmployeeEntity> findAllByDepartmentId(Long id);
}
