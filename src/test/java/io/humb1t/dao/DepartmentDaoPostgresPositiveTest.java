package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.*;

public class DepartmentDaoPostgresPositiveTest {
    private static DepartmentDao departmentDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        departmentDao = new DepartmentDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DROP_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            deleteDataStatement.executeUpdate();
        }
    }

    @Test
    public void createNewDepartment() throws ParseException {
        DepartmentEntity departmentEntity = new DepartmentEntity(6L, "Testing",
                new SimpleDateFormat("dd.mm.yyyy").parse("15.12.1999"));
        assertEquals(departmentEntity, departmentDao.create(departmentEntity));
    }

    @Test
    public void removeDepartmentReturnTrue() {
        assertTrue(departmentDao.remove(0L));
    }

    @Test
    public void removeDepartmentReturnFalse() {
        assertFalse(departmentDao.remove(6L));
    }

    @Test
    public void update() {
        String expectedName = "Sales";
        DepartmentEntity departmentEntity = departmentDao.find(1L);
        departmentEntity.setDeptName(expectedName);
        DepartmentEntity updatedDepartment = departmentDao.update(departmentEntity);
        assertEquals(expectedName, updatedDepartment.getDeptName());
    }

    @Test
    public void findAll() {
        int expectedCount = 5;
        List<DepartmentEntity> all = departmentDao.findAll();
        assertEquals(expectedCount, all.size());
    }

    @Test
    public void find() {
        DepartmentEntity departmentEntity = departmentDao.find(1L);
        assertNotNull(departmentEntity);
    }

    @Test
    public void findMostPopular() {
        int expectedSize = 2;
        List<DepartmentEntity> mostPopular = departmentDao.findMostPopular();
        assertEquals(expectedSize, mostPopular.size());
    }
}