package io.humb1t.utils.db;

import io.humb1t.exception.ExecutionException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;


public class RequestWrapper {

    private static final Logger LOGGER = Logger.getLogger(RequestWrapper.class);
    private static final String RESULT_INFO = "Result: %s";

    private RequestWrapper() {
        //util class
    }

    public static <T> T wrapEditingRequest(Connection connection, Executable<T> executable) {
        T result = null;
        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();
            result = executable.executeRequest(connection);
            connection.commit();
        } catch (RuntimeException e) {
            LOGGER.error("Error while execute editing query", e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error("Error while try to rollback", e1);
            }
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error while execute editing query", e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error("Error while try to rollback", e1);
            }
            throw new ExecutionException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("Error while closing connection", e);
            }
        }
        LOGGER.info(String.format(RESULT_INFO, result));
        return result;
    }

    public static <T> T wrapSelectingRequest(Connection connection, Executable<T> executable) {
        T result = null;
        try {
            result = executable.executeRequest(connection);
        } catch (RuntimeException e) {
            LOGGER.error("Error while execute selecting query", e);
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error while execute selecting query", e);
            throw new ExecutionException(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("Error while closing connection", e);
            }
        }
        LOGGER.info(String.format(RESULT_INFO, result));
        return result;
    }
}
