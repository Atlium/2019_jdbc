package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.domain.EmployeeEntity;
import io.humb1t.domain.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.exception.NotSingleResultException;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.ConnectionFactory;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoPostgres implements EmployeeDao {
    private static final Logger LOGGER = Logger.getLogger(EmployeeDaoPostgres.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";

    @Override
    public EmployeeEntity create(EmployeeEntity employeeEntity) {
        LOGGER.info(String.format("params:%s", employeeEntity));
        if (isNonValidEmployee(employeeEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating employee: %s", employeeEntity));
        final String SQL = "INSERT INTO employees VALUES(?, ?, ?, ?, ?)";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, employeeEntity.getId());
            preparedStatement.setDate(2, DateUtils.getSqlDate(employeeEntity.getEmploymentDate()));
            preparedStatement.setLong(3, employeeEntity.getMan().getId());
            preparedStatement.setLong(4, employeeEntity.getDepartment().getId());
            preparedStatement.setDate(5, DateUtils.getSqlDate(employeeEntity.getDismissDate()));
            if (preparedStatement.executeUpdate() > 0) {
                return employeeEntity;
            }
            throw new ExecutionException(String.format("Can't persist entity:%s", employeeEntity));
        });
    }

    @Override
    public boolean remove(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start removing employee with id: %d", id));
        final String SQL = "DELETE FROM employees WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate() > 0;
        });
    }

    @Override
    public EmployeeEntity update(EmployeeEntity employeeEntity) {
        LOGGER.info(String.format("params:%s", employeeEntity));
        if (isNonValidEmployee(employeeEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start updating employee: %s", employeeEntity));
        final String SQL = "UPDATE employees SET employment_date = ?, man = ?, department = ?, dismiss_date = ? WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setDate(1, DateUtils.getSqlDate(employeeEntity.getEmploymentDate()));
            preparedStatement.setLong(2, employeeEntity.getMan().getId());
            preparedStatement.setLong(3, employeeEntity.getDepartment().getId());
            preparedStatement.setDate(4, DateUtils.getSqlDate(employeeEntity.getDismissDate()));
            preparedStatement.setLong(5, employeeEntity.getId());
            if (preparedStatement.executeUpdate() > 0) {
                return employeeEntity;
            }
            throw new ExecutionException(String.format("Can't update entity:%s", employeeEntity));
        });
    }

    @Override
    public List<EmployeeEntity> findAll() {
        LOGGER.info("Start finding all employees");
        final String SQL = "SELECT id, employment_date, man, department, dismiss_date FROM employees";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<EmployeeEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new EmployeeEntity(rs.getLong("id"),
                        new java.util.Date(rs.getDate("employment_date").getTime()),
                        new ManEntity(rs.getLong("man")),
                        new DepartmentEntity(rs.getLong("department")),
                        DateUtils.getUtilDate(rs.getDate("dismiss_date"))));
            }
            return entities;
        });
    }

    @Override
    public EmployeeEntity find(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        final String SQL = "SELECT id, employment_date, man, department, dismiss_date FROM employees WHERE id = ?";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            try (ResultSet rs = preparedStatement.getResultSet()) {
                List<EmployeeEntity> entities = new ArrayList<>();
                while (rs.next()) {
                    entities.add(new EmployeeEntity(rs.getLong("id"),
                            DateUtils.getUtilDate(rs.getDate("employment_date")),
                            new ManEntity(rs.getLong("man")),
                            new DepartmentEntity(rs.getLong("department")),
                            DateUtils.getUtilDate(rs.getDate("dismiss_date"))));
                }
                if (entities.isEmpty()) {
                    throw new NotFoundException(String.format("Employee with id %s not found", id));
                } else if (entities.size() > 1) {
                    throw new NotSingleResultException(String.format("Found duplicated Employees with id %s", id));
                }
                return entities.get(0);
            }
        });
    }

    @Override
    public List<EmployeeEntity> findAllByDepartmentId(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        final String SQL = "{ CALL getEmployeesByDepartmentId(?) }";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            CallableStatement callableStatement = connection.prepareCall(SQL);
            callableStatement.setLong(1, id);
            callableStatement.execute();
            ResultSet rs = callableStatement.getResultSet();
            List<EmployeeEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new EmployeeEntity(rs.getLong("id"),
                        DateUtils.getUtilDate(rs.getDate("employment_date")),
                        new ManEntity(rs.getLong("man")),
                        new DepartmentEntity(rs.getLong("department")),
                        DateUtils.getUtilDate(rs.getDate("dismiss_date"))));
            }
            return entities;
        });
    }

    private boolean isNonValidEmployee(EmployeeEntity employeeEntity) {
        return employeeEntity == null
                || employeeEntity.getId() == null
                || employeeEntity.getEmploymentDate() == null
                || employeeEntity.getMan() == null
                || employeeEntity.getMan().getId() == null
                || employeeEntity.getDepartment() == null
                || employeeEntity.getDepartment().getId() == null;
    }
}
