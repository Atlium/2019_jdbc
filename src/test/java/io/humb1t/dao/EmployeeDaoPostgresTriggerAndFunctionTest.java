package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.domain.EmployeeEntity;
import io.humb1t.domain.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.assertEquals;

public class EmployeeDaoPostgresTriggerAndFunctionTest {
    private static EmployeeDao employeeDao;
    private static DepartmentDao departmentDao;
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        employeeDao = new EmployeeDaoPostgres();
        departmentDao = new DepartmentDaoPostgres();
        manDao = new ManDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createTables = connection.prepareStatement(CREATE_SCRIPT)) {
            createTables.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropTables = connection.prepareStatement(DROP_SCRIPT)) {
            dropTables.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createFunctions = connection.prepareStatement(CREATE_FUNC_SCRIPT);
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createFunctions.executeUpdate();
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropFunctions = connection.prepareStatement(DROP_FUNC_SCRIPT);
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            dropFunctions.executeUpdate();
            deleteDataStatement.executeUpdate();
        }
    }

    @Test(expected = ExecutionException.class)
    public void createWithHiringDateEarlierManBirthDate() {
        ManEntity manEntity = manDao.find(0L);
        DepartmentEntity departmentEntity = departmentDao.find(0L);
        Calendar c = Calendar.getInstance();
        c.setTime(manEntity.getBirthDate());
        c.add(Calendar.YEAR, -1);
        EmployeeEntity employeeEntity = new EmployeeEntity(6L, c.getTime(),
                manEntity, departmentEntity, null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void createWithDismissDateEarlierHiringDate() {
        ManEntity manEntity = manDao.find(0L);
        DepartmentEntity departmentEntity = departmentDao.find(3L);
        Calendar c = Calendar.getInstance();
        c.setTime(manEntity.getBirthDate());
        c.add(Calendar.YEAR, 18);
        Date employmentDate = c.getTime();
        c.add(Calendar.YEAR, -1);
        Date dismissDateDate = c.getTime();
        EmployeeEntity employeeEntity = new EmployeeEntity(6L, employmentDate,
                manEntity, departmentEntity, dismissDateDate);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void createWithHiringDateEarlierBirthDay() {
        ManEntity manEntity = manDao.find(0L);
        Calendar c = Calendar.getInstance();
        c.setTime(manEntity.getBirthDate());
        c.add(Calendar.YEAR, -1);
        Date employmentDate = c.getTime();
        EmployeeEntity employeeEntity = new EmployeeEntity(6L, employmentDate,
                manEntity, new DepartmentEntity(3L), null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void createWithHiringDateEarlierDepartmentFoundationDate() {
        DepartmentEntity departmentEntity = departmentDao.find(3L);
        Calendar c = Calendar.getInstance();
        c.setTime(departmentEntity.getFoundationDate());
        c.add(Calendar.YEAR, -1);
        Date employmentDate = c.getTime();
        EmployeeEntity employeeEntity = new EmployeeEntity(6L, employmentDate,
                new ManEntity(5L), departmentEntity, null);
        employeeDao.create(employeeEntity);
    }

    @Test
    public void findAllByDepartmentIdUsingStoredProcedure() {
        int expectedSize = 2;
        List<EmployeeEntity> employees = employeeDao.findAllByDepartmentId(1L);
        assertEquals(expectedSize, employees.size());
    }
}