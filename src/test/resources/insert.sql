INSERT INTO mans
VALUES
  (0, 'Ivanov', 'Ivan', '1991-01-01'),
  (1, 'Petrov', 'Petr', '1992-02-02'),
  (2, 'Vasin', 'Vasiliy', '1993-03-03'),
  (3, 'Stepanov', 'Stepan', '1994-04-04'),
  (4, 'Qwe', 'Q', '1994-04-04'),
  (5, 'Oldov', 'Old', '1951-08-11'),
  (6, 'Workerow', 'Worker', '1995-05-05');

INSERT INTO departments
VALUES
  (0, 'Developing', '1951-08-11'),
  (1, 'Security', '1962-09-12'),
  (2, 'Managing', '1984-11-14'),
  (3, 'Accounting', '1985-12-15'),
  (4, 'Marketing', '1973-10-13');

INSERT INTO employees
VALUES
  (0, '2018-05-11', 0, 0, '2019-05-11'),
  (1, '2018-06-12', 1, 1, NULL),
  (2, '2018-07-11', 2, 4, NULL),
  (3, '2018-08-13', 3, 1, NULL),
  (4, '2018-09-14', 4, 4, NULL);
