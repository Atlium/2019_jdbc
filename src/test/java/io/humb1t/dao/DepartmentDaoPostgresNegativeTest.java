package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.*;

public class DepartmentDaoPostgresNegativeTest {
    private static DepartmentDao departmentDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        departmentDao = new DepartmentDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DROP_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            deleteDataStatement.executeUpdate();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullDepartment() {
        departmentDao.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createDepartmentNullId() {
        DepartmentEntity department = new DepartmentEntity(null, "Doing", new Date());
        departmentDao.create(department);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createDepartmentNullName() {
        DepartmentEntity department = new DepartmentEntity(987L, null, new Date());
        departmentDao.create(department);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createDepartmentNullFoundationDate() {
        DepartmentEntity department = new DepartmentEntity(987L, "Doing", null);
        departmentDao.create(department);
    }

    @Test(expected = ExecutionException.class)
    public void createDepartmentExistingId() throws ParseException {
        DepartmentEntity departmentEntity = new DepartmentEntity(0L, "Testing",
                new SimpleDateFormat("dd.mm.yyyy").parse("15.12.1999"));
        departmentDao.create(departmentEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNullDepartment() {
        departmentDao.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullDepartment() {
        departmentDao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDepartmentNullId() {
        DepartmentEntity department = new DepartmentEntity(null, "Doing", new Date());
        departmentDao.create(department);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDepartmentNullName() {
        DepartmentEntity department = new DepartmentEntity(1L, null, new Date());
        departmentDao.create(department);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDepartmentNullFoundationDate() {
        DepartmentEntity department = new DepartmentEntity(1L, "Doing", null);
        departmentDao.create(department);
    }

    @Test(expected = ExecutionException.class)
    public void updateDepartmentNotExistingId() throws ParseException {
        DepartmentEntity departmentEntity = new DepartmentEntity(987L, "Testing",
                new SimpleDateFormat("dd.mm.yyyy").parse("15.12.1999"));
        departmentDao.update(departmentEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findNullMan() {
        departmentDao.find(null);
    }

    @Test(expected = NotFoundException.class)
    public void findNoOneMan() {
        departmentDao.find(957L);
    }
}