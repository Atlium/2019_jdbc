package io.humb1t.dao;

import io.humb1t.domain.ManEntity;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.*;

public class ManDaoPostgresPositiveTest {
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        manDao = new ManDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DROP_SCRIPT)) {
            preparedStatement.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            deleteDataStatement.executeUpdate();
        }
    }

    @Test
    public void createNewMan() throws ParseException {
        ManEntity manEntity = new ManEntity(698L, "qwe", "rty",
                new SimpleDateFormat("dd.mm.yyyy").parse("10.10.1990"));
        assertEquals(manDao.create(manEntity), manEntity);
    }

    @Test
    public void removeManReturnTrue() {
        assertTrue(manDao.remove(0L));
    }

    @Test
    public void removeManReturnFalse() {
        assertFalse(manDao.remove(8L));
    }

    @Test
    public void updateMan() {
        String expectedName = "qwe";
        ManEntity manEntity = manDao.find(1L);
        manEntity.setFirstName(expectedName);
        ManEntity updatedMan = manDao.update(manEntity);
        assertEquals(expectedName, updatedMan.getFirstName());
    }

    @Test
    public void findAllMan() {
        int expectedSize = 7;
        List<ManEntity> all = manDao.findAll();
        assertEquals(expectedSize, all.size());
    }

    @Test
    public void findMan() {
        ManEntity manEntity = manDao.find(1L);
        assertNotNull(manEntity);
    }

    @Test
    public void findAllUnemployed() {
        int expectedSize = 2;
        List<ManEntity> allUnemployed = manDao.findAllUnemployed();
        assertEquals(expectedSize, allUnemployed.size());
    }
}