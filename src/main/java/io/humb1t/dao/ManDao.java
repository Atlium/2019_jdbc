package io.humb1t.dao;

import io.humb1t.domain.ManEntity;

import java.util.List;

public interface ManDao {
    ManEntity create(ManEntity manEntity);

    boolean remove(Long id);

    ManEntity update(ManEntity manEntity);

    List<ManEntity> findAll();

    ManEntity find(Long id);

    List<ManEntity> findAllUnemployed();
}
