package io.humb1t.dao;

import io.humb1t.domain.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.exception.NotSingleResultException;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.ConnectionFactory;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class ManDaoPostgres implements ManDao {

    private static final Logger LOGGER = Logger.getLogger(ManDaoPostgres.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";

    @Override
    public ManEntity create(ManEntity manEntity) {
        LOGGER.info(String.format("params:%s", manEntity));
        if (isNonValidMan(manEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating man: %s", manEntity));
        final String SQL = "INSERT INTO mans VALUES(?, ?, ?, ?)";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, manEntity.getId());
            preparedStatement.setString(2, manEntity.getFirstName());
            preparedStatement.setString(3, manEntity.getSecondName());
            preparedStatement.setDate(4, new Date(manEntity.getBirthDate().getTime()));
            if (preparedStatement.executeUpdate() > 0) {
                return manEntity;
            }
            throw new ExecutionException(String.format("Can't persist entity:%s", manEntity));
        });
    }

    @Override
    public boolean remove(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start removing man with id: %d", id));
        final String SQL = "DELETE FROM mans WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            boolean result = preparedStatement.executeUpdate() > 0;
            return result;
        });
    }

    @Override
    public ManEntity update(ManEntity manEntity) {
        LOGGER.info(String.format("params:%s", manEntity));
        if (isNonValidMan(manEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start updating man: %s", manEntity));
        final String SQL = "UPDATE mans SET first_name = ?, second_name = ?, birth_date = ? WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, manEntity.getFirstName());
            preparedStatement.setString(2, manEntity.getSecondName());
            preparedStatement.setDate(3, new Date(manEntity.getBirthDate().getTime()));
            preparedStatement.setLong(4, manEntity.getId());
            if (preparedStatement.executeUpdate() > 0) {
                return manEntity;
            }
            throw new ExecutionException(String.format("Can't update entity:%s", manEntity));
        });
    }

    @Override
    public List<ManEntity> findAll() {
        LOGGER.info("Start finding all mans");
        final String SQL = "SELECT id, first_name, second_name, birth_date FROM mans";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<ManEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new ManEntity(rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("second_name"),
                        new java.util.Date(rs.getDate("birth_date").getTime())));
            }
            return entities;
        });
    }

    @Override
    public ManEntity find(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        final String SQL = "SELECT id, first_name, second_name, birth_date FROM mans WHERE id = ?";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<ManEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new ManEntity(rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("second_name"),
                        DateUtils.getUtilDate(rs.getDate("birth_date"))));
            }
            if (entities.isEmpty()) {
                throw new NotFoundException(String.format("Man with id %s not found", id));
            } else if (entities.size() > 1) {
                throw new NotSingleResultException(String.format("Found duplicated Mans with id %s", id));
            }
            ManEntity manEntity = entities.get(0);
            return manEntity;
        });
    }

    @Override
    public List<ManEntity> findAllUnemployed() {
        LOGGER.info("Start finding all unemployed mans");
        final String SQL = "SELECT m.id, m.first_name, m.second_name, m.birth_date " +
                "FROM mans m " +
                "LEFT JOIN employees e ON e.man = m.id " +
                "WHERE e.department IS NULL";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<ManEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new ManEntity(rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("second_name"),
                        DateUtils.getUtilDate(rs.getDate("birth_date"))));
            }
            return entities;
        });
    }

    private boolean isNonValidMan(ManEntity man) {
        return man == null
                || man.getId() == null
                || man.getBirthDate() == null
                || man.getFirstName() == null
                || man.getSecondName() == null;
    }
}
