CREATE OR REPLACE FUNCTION check_employee_dismissed()
  RETURNS TRIGGER AS
$cua$
BEGIN
  IF OLD.dismiss_date IS NULL
  THEN
    RAISE EXCEPTION 'Employee cannot be deleted while he is not dismissed';
  END IF;
  RETURN OLD;
END;
$cua$
LANGUAGE plpgsql;

CREATE TRIGGER on_delete_department
  BEFORE DELETE
  ON employees
  FOR EACH ROW
EXECUTE PROCEDURE check_employee_dismissed();

CREATE OR REPLACE FUNCTION getEmployeesByDepartmentId(department_id BIGINT)
  RETURNS TABLE(
    id              BIGINT,
    employment_date TIMESTAMP,
    man             BIGINT,
    department      BIGINT,
    dismiss_date    TIMESTAMP
  ) AS
$$
SELECT
  e.id,
  e.employment_date,
  e.man,
  e.department,
  e.dismiss_date
FROM employees e
WHERE e.department = department_id
$$
LANGUAGE sql;