package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.domain.EmployeeEntity;
import io.humb1t.domain.ManEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;

import static io.humb1t.dao.DbConfig.*;

public class EmployeeDaoPostgresNegativeTest {
    private static EmployeeDao employeeDao;
    private static DepartmentDao departmentDao;
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        employeeDao = new EmployeeDaoPostgres();
        departmentDao = new DepartmentDaoPostgres();
        manDao = new ManDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createTables = connection.prepareStatement(CREATE_SCRIPT)) {
            createTables.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropTables = connection.prepareStatement(DROP_SCRIPT)) {
            dropTables.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createFunctions = connection.prepareStatement(CREATE_FUNC_SCRIPT);
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createFunctions.executeUpdate();
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropFunctions = connection.prepareStatement(DROP_FUNC_SCRIPT);
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            dropFunctions.executeUpdate();
            deleteDataStatement.executeUpdate();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNullEmployee() {
        employeeDao.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmployeeNullId() {
        EmployeeEntity employeeEntity = new EmployeeEntity(null, new Date(),
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void createEmployeeExistingId() {
        EmployeeEntity employeeEntity = new EmployeeEntity(1L, new Date(),
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmployeeNullHiringDate() {
        EmployeeEntity employeeEntity = new EmployeeEntity(987L, null,
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmployeeNullDepartment() {
        EmployeeEntity employeeEntity = new EmployeeEntity(987L, new Date(),
                new ManEntity(1L), null, null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmployeeNullMan() {
        EmployeeEntity employeeEntity = new EmployeeEntity(987L, new Date(),
                null, new DepartmentEntity(1L), null);
        employeeDao.create(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullEmployee() {
        employeeDao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateEmployeeNullId() {
        EmployeeEntity employeeEntity = new EmployeeEntity(null, new Date(),
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.update(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void updateNotExistedEmployee() {
        EmployeeEntity employeeEntity = new EmployeeEntity(987L, new Date(),
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.update(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateEmployeeNullHiringDate() {
        EmployeeEntity employeeEntity = new EmployeeEntity(1L, null,
                new ManEntity(1L), new DepartmentEntity(1L), null);
        employeeDao.update(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateEmployeeNullDepartment() {
        EmployeeEntity employeeEntity = new EmployeeEntity(1L, new Date(),
                new ManEntity(1L), null, null);
        employeeDao.update(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateEmployeeNullMan() {
        EmployeeEntity employeeEntity = new EmployeeEntity(1L, new Date(),
                null, new DepartmentEntity(1L), null);
        employeeDao.update(employeeEntity);
    }

    @Test(expected = ExecutionException.class)
    public void updateEmployeeHiringDateEarlierDismissDate() {
        EmployeeEntity employeeEntity = employeeDao.find(1L);
        Date employmentDate = employeeEntity.getEmploymentDate();
        Calendar c = Calendar.getInstance();
        c.setTime(employmentDate);
        c.add(Calendar.DATE, -1);
        employeeEntity.setDismissDate(c.getTime());
        employeeDao.update(employeeEntity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByNullId() {
        employeeDao.find(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findAllByNullDepartmentId() {
        employeeDao.findAllByDepartmentId(null);
    }
}