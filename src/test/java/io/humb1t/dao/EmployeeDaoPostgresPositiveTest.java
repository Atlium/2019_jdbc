package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.domain.EmployeeEntity;
import io.humb1t.domain.ManEntity;
import io.humb1t.utils.db.ConnectionFactory;
import org.junit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

import static io.humb1t.dao.DbConfig.*;
import static org.junit.Assert.*;

public class EmployeeDaoPostgresPositiveTest {
    private static EmployeeDao employeeDao;
    private static DepartmentDao departmentDao;
    private static ManDao manDao;

    @BeforeClass
    public static void beforeClass() throws Exception {
        employeeDao = new EmployeeDaoPostgres();
        departmentDao = new DepartmentDaoPostgres();
        manDao = new ManDaoPostgres();
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createTables = connection.prepareStatement(CREATE_SCRIPT)) {
            createTables.executeUpdate();
        }
    }

    @AfterClass
    public static void afterClass() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropTables = connection.prepareStatement(DROP_SCRIPT)) {
            dropTables.executeUpdate();
        }
    }

    @Before
    public void initialInsert() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement createFunctions = connection.prepareStatement(CREATE_FUNC_SCRIPT);
             PreparedStatement createDataStatement = connection.prepareStatement(INSERT_SCRIPT)) {
            createFunctions.executeUpdate();
            createDataStatement.executeUpdate();
        }
    }

    @After
    public void clearAfterTest() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement dropFunctions = connection.prepareStatement(DROP_FUNC_SCRIPT);
             PreparedStatement deleteDataStatement = connection.prepareStatement(DELETE_SCRIPT)) {
            dropFunctions.executeUpdate();
            deleteDataStatement.executeUpdate();
        }
    }

    @Test
    public void create() {
        ManEntity manEntity = manDao.find(0L);
        DepartmentEntity departmentEntity = departmentDao.find(0L);
        EmployeeEntity newEmployee = new EmployeeEntity(10L, new Date(), manEntity, departmentEntity, null);
        EmployeeEntity createdEmployee = employeeDao.create(newEmployee);
        assertEquals(newEmployee, createdEmployee);
    }

    @Test
    public void removeReturnTrue() {
        assertTrue(employeeDao.remove(0L));
    }

    @Test
    public void removeReturnFalse() {
        assertFalse(employeeDao.remove(987L));
    }

    @Test
    public void update() {
        EmployeeEntity employeeEntity = employeeDao.find(0L);
        Date dismissDate = new Date();
        employeeEntity.setDismissDate(dismissDate);
        EmployeeEntity updatedEmployee = employeeDao.update(employeeEntity);
        assertEquals(dismissDate, updatedEmployee.getDismissDate());
    }

    @Test
    public void findAll() {
        int expectedSize = 5;
        List<EmployeeEntity> all = employeeDao.findAll();
        assertEquals(expectedSize, all.size());
    }

    @Test
    public void find() {
        assertNotNull(employeeDao.find(0L));
    }

    @Test
    public void findAllByDepartmentId() {
        int expectedSize = 2;
        List<EmployeeEntity> allByDepartmentId = employeeDao.findAllByDepartmentId(1L);
        assertEquals(expectedSize, allByDepartmentId.size());
    }
}