package io.humb1t.utils.db;

import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.*;

public class ConnectionFactoryTest {

    @Test
    public void getConnection() throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            assertNotNull(connection);
        }
    }
}