package io.humb1t.suit;

import io.humb1t.dao.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ManDaoPostgresPositiveTest.class, ManDaoPostgresNegativeTest.class,
        DepartmentDaoPostgresPositiveTest.class, DepartmentDaoPostgresNegativeTest.class,
        EmployeeDaoPostgresPositiveTest.class, EmployeeDaoPostgresNegativeTest.class,
        EmployeeDaoPostgresTriggerAndFunctionTest.class})
public class DaoTestSuite {
}
