package io.humb1t.domain;

import io.humb1t.utils.date.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class ManEntity implements Serializable {

    private static final long serialVersionUID = -5186864720164264965L;
    private Long id;
    private String firstName;
    private String secondName;
    private Date birthDate;

    public ManEntity() {
    }

    public ManEntity(Long id) {
        this.id = id;
    }

    public ManEntity(Long id, String firstName, String secondName, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ManEntity)) return false;
        ManEntity manEntity = (ManEntity) o;
        return Objects.equals(getId(), manEntity.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ManEntity{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthDate=" + DateUtils.formatToTimestamp(birthDate) +
                '}';
    }
}
