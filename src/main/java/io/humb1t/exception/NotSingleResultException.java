package io.humb1t.exception;

public class NotSingleResultException extends RuntimeException {
    public NotSingleResultException(String message) {
        super(message);
    }

    public NotSingleResultException(String message, Throwable cause) {
        super(message, cause);
    }
}
