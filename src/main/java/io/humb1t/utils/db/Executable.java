package io.humb1t.utils.db;

import java.sql.Connection;

public interface Executable<T> {
    T executeRequest(Connection connection) throws Exception;
}
