package io.humb1t.dao;

import io.humb1t.domain.DepartmentEntity;
import io.humb1t.exception.ExecutionException;
import io.humb1t.exception.NotFoundException;
import io.humb1t.exception.NotSingleResultException;
import io.humb1t.utils.date.DateUtils;
import io.humb1t.utils.db.ConnectionFactory;
import io.humb1t.utils.db.RequestWrapper;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class DepartmentDaoPostgres implements DepartmentDao {

    private static final Logger LOGGER = Logger.getLogger(DepartmentDaoPostgres.class);
    private static final String NULL_VALUE_MESSAGE = "Null value";

    @Override
    public DepartmentEntity create(DepartmentEntity departmentEntity) {
        LOGGER.info(String.format("params:%s", departmentEntity));
        if (isNonValidDepartment(departmentEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start creating department: %s", departmentEntity));
        final String SQL = "INSERT INTO departments VALUES(?, ?, ?)";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, departmentEntity.getId());
            preparedStatement.setString(2, departmentEntity.getDeptName());
            preparedStatement.setDate(3, new Date(departmentEntity.getFoundationDate().getTime()));
            if (preparedStatement.executeUpdate() > 0) {
                return departmentEntity;
            }
            throw new ExecutionException(String.format("Can't persist entity:%s", departmentEntity));
        });
    }

    @Override
    public boolean remove(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start removing department with id: %d", id));
        final String SQL = "DELETE FROM departments WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate() > 0;
        });
    }

    @Override
    public DepartmentEntity update(DepartmentEntity departmentEntity) {
        LOGGER.info(String.format("params:%s", departmentEntity));
        if (isNonValidDepartment(departmentEntity)) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        LOGGER.info(String.format("Start updating department: %s", departmentEntity));
        final String SQL = "UPDATE departments SET dept_name = ?, foundation_date = ? WHERE id = ?";
        return RequestWrapper.wrapEditingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, departmentEntity.getDeptName());
            preparedStatement.setDate(2, new Date(departmentEntity.getFoundationDate().getTime()));
            preparedStatement.setLong(3, departmentEntity.getId());
            if (preparedStatement.executeUpdate() > 0) {
                return departmentEntity;
            }
            throw new ExecutionException(String.format("Can't update entity:%s", departmentEntity));
        });
    }

    @Override
    public List<DepartmentEntity> findAll() {
        LOGGER.info("Start finding all departments");
        final String SQL = "SELECT id, dept_name, foundation_date FROM departments";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<DepartmentEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new DepartmentEntity(rs.getLong("id"),
                        rs.getString("dept_name"),
                        DateUtils.getUtilDate(rs.getDate("foundation_date"))));
            }
            return entities;
        });
    }

    @Override
    public DepartmentEntity find(Long id) {
        LOGGER.info(String.format("params:%s", id));
        if (id == null) {
            throw new IllegalArgumentException(NULL_VALUE_MESSAGE);
        }
        final String SQL = "SELECT id, dept_name, foundation_date FROM departments WHERE id = ?";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<DepartmentEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new DepartmentEntity(rs.getLong("id"),
                        rs.getString("dept_name"),
                        DateUtils.getUtilDate(rs.getDate("foundation_date"))));
            }
            if (entities.isEmpty()) {
                throw new NotFoundException(String.format("Department with id %s not found", id));
            } else if (entities.size() > 1) {
                throw new NotSingleResultException(String.format("Found duplicated Departments with id %s", id));
            }
            return entities.get(0);
        });
    }

    @Override
    public List<DepartmentEntity> findMostPopular() {
        LOGGER.info("Start finding most popular departments");
        final String SQL = "SELECT d.id, d.dept_name, d.foundation_date " +
                "FROM departments d " +
                "JOIN employees e ON e.department = d.id " +
                "GROUP BY d.id " +
                "HAVING COUNT(e.id) = (" +
                "   SELECT COUNT(e.id) total_count " +
                "   FROM departments d " +
                "   JOIN employees e ON e.department = d.id " +
                "   GROUP BY d.id " +
                "   ORDER BY total_count DESC " +
                "   LIMIT 1" +
                ")";
        return RequestWrapper.wrapSelectingRequest(ConnectionFactory.getConnection(), connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            List<DepartmentEntity> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(new DepartmentEntity(rs.getLong("id"),
                        rs.getString("dept_name"),
                        DateUtils.getUtilDate(rs.getDate("foundation_date"))));
            }
            return entities;
        });
    }

    private boolean isNonValidDepartment(DepartmentEntity department) {
        return department == null
                || department.getId() == null
                || department.getDeptName() == null
                || department.getFoundationDate() == null;
    }
}
